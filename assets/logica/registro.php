<header>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</header>
<style>
    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
</style>
<?php
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");

$nombre=$_POST['nombre'];
$apellido=$_POST['apellido'];
$correo=$_POST['correo'];
$telefono=$_POST['telefono'];
$tipoSangre=$_POST['tipoSangre'];
$dni=$_POST['dni'];
$ciudad=$_POST['ciudad'];
$distrito=$_POST['distrito'];
$contrasena=$_POST['contrasena'];


$insertar="INSERT INTO usuarios(`id_rol`, `nombre`, `apellido`, `email`, `telefono`, `tipo_sangre`, `dni`, `ciudad`, `distrito`, `contrasena`) VALUES
(3,'$nombre', '$apellido', '$correo', '$telefono', '$tipoSangre', '$dni', '$ciudad', '$distrito', '$contrasena');";

$verificarUsuarioDuplicado=mysqli_query($conexion, "SELECT * FROM usuarios WHERE email='$correo'");
if (mysqli_num_rows($verificarUsuarioDuplicado)>0) {
    echo'<script>
    Swal.fire({
     icon: "warning",
     title: "Ya existe un usuario con ese correo",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
            window.history.go(-1);
        }
     });
    </script>';
    exit;
}

$resultado=mysqli_query($conexion, $insertar);
if (!$resultado) {
    echo'<script>
    Swal.fire({
     icon: "error",
     title: "Error al registrarse",
     text: "Intentalo nuevamente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "../../registro.html";
        }
     });
    </script>';
} else {
    echo'<script>
    Swal.fire({
     icon: "success",
     title: "Su cuenta ha sido creada con Éxito",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "../../login.html";
        }
     });
    </script>';
}

mysqli_close($conexion);
?>