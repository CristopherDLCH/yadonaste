<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];

$consulta="SELECT * FROM solicitudes ORDER BY id_solicitudes DESC";

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title> 
    <link rel="stylesheet" href="../css/Dashboards.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil1.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboardadmi.php"><i class="fas fa-sliders-h"></i><span>Solicitudes en espera</span></a>
        <a href="dashboardadmiSolicitudInsert.php"><i class="fas fa-sliders-h"></i><span>Crear solicitud</span></a>
        <a href="dashboardadmiSolicitudes.php"><i class="fas fa-sliders-h"></i><span>Solicitudes</span></a>
        <a href="dashboardadmiUsuariosInsert.php"><i class="fas fa-cogs"></i><span>Crear usuario</span></a>
        <a href="dashboardadmiUsuarios.php"><i class="fas fa-cogs"></i><span>Usuarios</span></a>
      </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
        <h4>Administrador</h4>
      </div>
        <a href="dashboardadmi.php"><i class="fas fa-sliders-h"></i><span>Solicitudes en espera</span></a>
        <a href="dashboardadmiSolicitudInsert.php"><i class="fas fa-sliders-h"></i><span>Crear solicitud</span></a>
        <a href="dashboardadmiSolicitudes.php"><i class="fas fa-sliders-h"></i><span>Solicitudes</span></a>
        <a href="dashboardadmiUsuariosInsert.php"><i class="fas fa-cogs"></i><span>Crear usuario</span></a>
        <a href="dashboardadmiUsuarios.php"><i class="fas fa-cogs"></i><span>Usuarios</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <div class="card">
        <h1>Solicitudes</h1>
      </div>
      <div class="mitos">
        <?php $resultado=mysqli_query($conexion, $consulta);
        while ($row=mysqli_fetch_assoc($resultado)) {
        ?>
          <div class="mitosPartes"> 
              <img class="imagen" src="../img/fotoPerfil3.png" alt="">
              <h3><?php echo $row["nombre"], " ", $row["apellido"], " - ", $row["edad"], " años";?></h3>
              <p>Requiere sangre de tipo <strong><?php echo $row["tipo_sangre"]; ?></p>
              <p>Necesita <strong><?php echo $row["cantidad_donantes"]; ?> donantes</p>
              <br>
              <a class="contacto" type="button" class="fadeIn fourth" href="dashboardadmiSolicitudesUpdate.php?id=<?php echo $row["id_solicitudes"];?>" value="">Visualizar todo</a>
          </div>

        <?php } mysqli_free_result($resultado);?>
    </div>

        

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });

    
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  </body>
</html>