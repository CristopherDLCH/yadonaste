<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];
$id_usuarios=$_GET['id'];

$consultaa="SELECT * FROM usuarios WHERE id_usuarios='$id_usuarios'";
$resultado=mysqli_query($conexion, $consultaa);
$row=mysqli_fetch_assoc($resultado);

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title>
    <link rel="stylesheet" href="../css/styleEditarMisDatoss.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil1.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
        <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
        <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
        <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
      </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
      </div>
      <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
      <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
      <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
      <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <div class="card">
          <h1>Actualiza tus datos</h1>
      </div>
      <div class="mitos">
        <div class="mitosPartes"> 
          <form class= "fo" action="datosUserEditUpdate.php" method="POST">
            <input type="hidden" id="login" class="uno" name="id_usuarios" value="<?php echo $row["id_usuarios"]?>">
            <input type="hidden" id="login" class="uno" name="id_rol" value="<?php echo $row["id_rol"]?>">
            <br>
            <label class="lb3">Nombre</label>
            <label class="lb3">Apellidos</label>
            <label class="lb3">Correo</label>
            <br>
            <input type="text" id="login" class="dos" name="nombre" onkeypress="return SoloLetras(event);" value="<?php echo $row["nombre"]?>">
            <input type="text" id="login" class="dos" name="apellido" onkeypress="return SoloLetras(event);" value="<?php echo $row["apellido"]?>">
            <input type="text" id="login" class="dos" name="correo" value="<?php echo $row["email"]?>" readonly>
            <br>
            <label class="lb2">Teléfono</label>
            <label class="lb2">Tipo de sangre</label>
            <label class="lb2">DNI</label>
            <br>
            <input type="text" id="login" class="dos validar" minlength="9" maxlength="9" name="telefono" value="<?php echo $row["telefono"]?>">
            <select type="text" id="login" class="selectt" name="tipo_sangre" >
            <option value="<?php echo $row["tipo_sangre"]?>"><?php echo $row["tipo_sangre"]?></option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>   
             </select>    
            <input type="text" id="login" class="dos" name="dni" value="<?php echo $row["dni"]?>" readonly>
            <br>
            <label class="lb3">Ciudad</label>
            <label class="lb3">Distrito</label>
            <br>
            <input type="text" id="login" class="dos" name="ciudad" value="<?php echo $row["ciudad"]?>" readonly>
            <select type="text" id="login" class="selectt" name="distrito" placeholder="Distrito"required>
              <option value="<?php echo $row["distrito"]?>"><?php echo $row["distrito"]?></option>
              <option value="Cercado de Lima">Cercado de Lima</option>
              <option value="Ate">Ate</option>
              <option value="Barranco">Barranco</option>
              <option value="Breña">Breña</option>
              <option value="Comas">Comas</option>
              <option value="Chorrillos">Chorrillos</option>
              <option value="Agustino">El Agustino</option>
              <option value="Jesús María">Jesús María</option>
              <option value="La Molina">La Molina</option>
              <option value="La Victoria">La Victoria</option>
              <option value="Lince">Lince</option>
              <option value="Magdalena">Magdalena</option>
              <option value="Miraflores">Miraflores</option>
              <option value="Pueblo Libre">Pueblo Libre</option>
              <option value="Puente Piedra">Puente Piedra</option>
              <option value="Rimac">Rimac</option>
              <option value="San Isidro">San Isidro</option>
              <option value="Independencia">Independencia</option>
              <option value="San Juan de Miraflores">San Juan de Miraflores</option>
              <option value="San Luis">San Luis</option>
              <option value="San Martin de Porres">San Martin de Porres</option>
              <option value="San Miguel">San Miguel</option>
              <option value="Santiago de Surco">Santiago de Surco</option>
              <option value="Surquillo">Surquillo</option>
              <option value="Villa María del Triunfo">Villa María del Triunfo</option>
              <option value="San Juan de Lurigancho">San Juan de Lurigancho</option>
              <option value="Santa Rosa">Santa Rosa</option>
              <option value="Los Olivos">Los Olivos</option>
              <option value="San Borja">San Borja</option>
              <option value="Villa El Savador">Villa El Savador</option>
              <option value="Santa Anita">Santa Anita</option>
            </select>
            <br>
            <input type="submit" class="fadeIn fourth" value="Actualizar">
          </form>
        </div> 
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });

    $(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});

function SoloLetras(e)
    {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toString();
      letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚñÑ ";

      especiales = [8,13];
      tecla_especial = false
      for(var i in especiales) {
      if(key == especiales[i]){
      tecla_especial = true;
      break;
      }
      }

      if(letras.indexOf(tecla) == -1 && !tecla_especial)
      {
      alert("Ingresar solo letras");
      return false;
      }
    }
    </script>

  </body>
</html>