<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];

$consulta="SELECT id_usuarios FROM usuarios WHERE email='$varSesion'";
$resultado=mysqli_query($conexion, $consulta);
$row=mysqli_fetch_assoc($resultado);
$ultimate=$row["id_usuarios"];

$consulta2="SELECT * FROM usuarios WHERE id_usuarios='$ultimate'";
$resultado2=mysqli_query($conexion, $consulta2);

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title>
    <link rel="stylesheet" href="../css/styleMisDatos.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil1.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
        <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
        <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
        <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
      </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
      </div>
      <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
      <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
      <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
      <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <br>
      <div class="card">
        <h3>Mis datos personales</h3>
      </div>
      <br>
      <div class="mitos">
        <?php
        while ($row2=mysqli_fetch_assoc($resultado2)) {
        ?>
        <br>
        <br>
          <div class="mitosPartes"> 
              <img class="imagen" src="../img/fotoPerfil2.png" alt="">
              <h3></h3>
              <p>Nombre: <strong><?php echo $row2["nombre"];?></strong></p>
              <p>Apellido: <strong><?php echo $row2["apellido"];?></strong></p>
              <p>Correo: <strong><?php echo $row2["email"];?></strong></p>
              <p>Teléfono: <strong><?php echo $row2["telefono"];?></strong></p>
              <p>Tipo de sangre: <strong><?php echo $row2["tipo_sangre"];?></strong></p>
              <p>DNI: <strong><?php echo $row2["dni"];?></strong></p>
              <p>Ciudad: <strong><?php echo $row2["ciudad"];?></strong></p>
              <p>Distrito: <strong><?php echo $row2["distrito"];?></strong></p>
              <!--<p>Contraseña: <strong>**********</strong></p>-->
              <br>
              <a class="contacto-green" type="button" class="fadeIn fourth" href="datosUserEdit.php?id=<?php echo $row2["id_usuarios"];?>">Editar datos</a>
              <a class="contacto-red" type="button" class="fadeIn fourth" href="datosUserContrasena.php?id=<?php echo $row2["id_usuarios"];?>">Cambiar Contraseña</a>
            </div>
        <?php } mysqli_free_result($resultado2);?>
    </div>
    
    <script src="assets/js/eliminarConfirmacion.js"></script>

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });
    </script>

  </body>
</html>