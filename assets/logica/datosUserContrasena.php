<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];
$id_usuarios=$_GET['id'];

$consultaa="SELECT * FROM usuarios WHERE id_usuarios='$id_usuarios'";
$resultado=mysqli_query($conexion, $consultaa);
$row=mysqli_fetch_assoc($resultado);

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title>
    <link rel="stylesheet" href="../css/styleDatosUserContrasena.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil1.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
        <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
        <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
        <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
      </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
      </div>
      <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
      <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
      <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
      <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <div class="card">
          <h1>Cambiar Contraseña</h1>
      </div>
      <div class="mitos">
        <div class="mitosPartes"> 
          <form class= "fo" action="datosUserContrasenaUpdate.php" method="POST" id="frmRegistro">

            <input type="hidden" id="login" class="uno" name="id_usuarios" value="<?php echo $row["id_usuarios"]?>">
            <br>
            <input type="password" id="login" class="dos" name="contrasenaActual" placeholder="Contraseña actual" onkeypress="" value="" required>
            <input type="password" id="pass1" class="dos" name="contrasenaNueva" placeholder="Nueva contraseña" onkeypress="" value="" required>
            <input type="password" id="pass2" class="dos" name="contrasenaNuevaRepetir" placeholder="Repetir nueva contraseña" value="" required>
            <br>
            <span id="error2"></span>
            <br>
            <input type="submit" class="fadeIn fourth" value="Actualizar" id="btn" disabled>
            <br>
          </form>
        </div> 
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });

    $(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});

function SoloLetras(e)
    {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toString();
      letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚñÑ ";

      especiales = [8,13];
      tecla_especial = false
      for(var i in especiales) {
      if(key == especiales[i]){
      tecla_especial = true;
      break;
      }
      }

      if(letras.indexOf(tecla) == -1 && !tecla_especial)
      {
      alert("Ingresar solo letras");
      return false;
      }
    }
    </script>

    <script>
      $(document).ready(function(){
        $('#pass2').keyup(function(){

            var pass1 = $('#pass1').val();
            var pass2 = $('#pass2').val();

            if ( pass1 == pass2 ) {
              $('#error2').text("Coinciden!").css("color", "green");
              document.getElementById("btn").disabled = false;
            } else {
              $('#error2').text("Debe coincidir con tu contraseña nueva.").css("color", "red");
              document.getElementById("btn").disabled = true;
            }
            if (pass2 == "") {
              $('#error2').text("No se puede dejar en blanco!").css("color", "red");
              document.getElementById("btn").disabled = true;
            }
        })
      })
    </script>
  </body>
</html>