<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];
$id_solicitudes=$_GET['id'];

$consulta1="SELECT * FROM solicitudes WHERE id_solicitudes='$id_solicitudes'";
$resultado1=mysqli_query($conexion, $consulta1);

$consulta2="SELECT id_usuarios FROM usuarios WHERE email='$varSesion'";
$resultado2=mysqli_query($conexion, $consulta2);
$row=mysqli_fetch_assoc($resultado2);
$ultimate=$row["id_usuarios"];

$consulta3="SELECT * FROM usuarios WHERE id_usuarios='$ultimate'";
$resultado3=mysqli_query($conexion, $consulta3);

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title> 
    <link rel="stylesheet" href="../css/styleSolicitudesVerMas.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil1.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
        <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
        <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
        <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
        </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
      </div>
        <a href="dashboard.php"><i class="fas fa-desktop"></i><span>Quiero donar</span></a>
        <a href="solicitudes.php"><i class="fas fa-cogs"></i><span>Necesito donantes</span></a>
        <a href="solicitudesUser.php"><i class="fas fa-cogs"></i><span>Mis solicitudes</span></a>
        <a href="datosUser.php"><i class="fas fa-sliders-h"></i><span>Mis Datos</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <div class="card">
        <h2>.</h2>
      </div>
      <div class="mitos">
          <div class="mitosPartes"> 
            <form class= "fo" action="donar.php" method="POST">
                <img class="imagen" src="../img/fotoPerfil3.png" alt="">
                <?php
                    while ($row=mysqli_fetch_assoc($resultado1)) {
                ?>
                <input type="hidden" id="login" class="uno" name="id_solicitudes" value="<?php echo $row["id_solicitudes"]?>">
                <p><strong><?php echo $row["nombre"]," "; ?></strong><strong><?php echo $row["apellido"]; ?></strong></p>
                <p></p>
                <p>Edad: <?php echo $row["edad"]; ?></p>
                <p>Tipo de sangre: <?php echo $row["tipo_sangre"]; ?></p>
                <p>DNI: <?php echo $row["dni"]; ?></p>
                <p>Ciudad: <?php echo $row["ciudad"]; ?></p>
                <p>Distrito: <?php echo $row["distrito"]; ?></p>
                <p>Hospital: <?php echo $row["hospital_donacion"]; ?></p>
                <p>Descripción: <?php echo $row["descripcion"]; ?></p>
                <p>Teléfono: <?php echo $row["telefono"]; ?></p>
                <br>
                <?php } mysqli_free_result($resultado1);?>
                
                <?php
                    while ($row2=mysqli_fetch_assoc($resultado3)) {
                ?>
                <input type="hidden" id="login" class="dos" name="nombre2" onkeypress="" value="<?php echo $row2["nombre"]?>">
                <input type="hidden" id="login" class="dos" name="apellido2" onkeypress="" value="<?php echo $row2["apellido"]?>">
                <input type="hidden" id="login" class="dos" name="email2" onkeypress="" value="<?php echo $row2["email"]?>">
                <input type="hidden" id="login" class="dos" name="telefono2" onkeypress="" value="<?php echo $row2["telefono"]?>">
                <input type="hidden" id="login" class="dos" name="tipo_sangre2" onkeypress="" value="<?php echo $row2["tipo_sangre"]?>">
                <input type="hidden" id="login" class="dos" name="dni2" onkeypress="" value="<?php echo $row2["dni"]?>">
                <input type="hidden" id="login" class="dos" name="ciudad2" onkeypress="" value="<?php echo $row2["ciudad"]?>">
                <input type="hidden" id="login" class="dos" name="distrito2" onkeypress="" value="<?php echo $row2["distrito"]?>">
                <?php } mysqli_free_result($resultado3);?>
                <input type="submit" class="contacto-postular" value="Postular">
                <a class="contacto-red" type="button" class="fadeIn fourth" href="dashboard.php" value="">Cancelar</a>
            </form>
        </div>
    </div>

        

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });

    
    </script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  </body>
</html>