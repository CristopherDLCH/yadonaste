<header>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</header>
<style>
    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
</style>
<?php
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");

$idUsuario=$_POST['idUsuarios'];
$nombre=$_POST['nombre'];
$apellido=$_POST['apellido'];
$edad=$_POST['edad'];
$tipo_sangre=$_POST['tipoSangre'];
$cantidad_donantes=$_POST['cantidadDonantes'];
$dni=$_POST['dni'];
$ciudad=$_POST['ciudad'];
$distrito=$_POST['distrito'];
$hospital=$_POST['hospital'];
$descripcion=$_POST['descripcion'];
$telefono=$_POST['telefono'];

$insertar="INSERT INTO solicitudes(`id_usuarios`, `nombre`, `apellido`, `edad`, `tipo_sangre`, `cantidad_donantes`, `dni`, `ciudad`, `distrito`, `hospital_donacion`, `descripcion`, `telefono`, `estado`, `proceso`) VALUES
('$idUsuario', '$nombre', '$apellido', '$edad', '$tipo_sangre', '$cantidad_donantes', '$dni', '$ciudad', '$distrito', '$hospital', '$descripcion', '$telefono', 'No aprobado', 'Incompleto');";

$verificarSolicitudDuplicada=mysqli_query($conexion, "SELECT * FROM solicitudes WHERE dni='$dni'");
if (mysqli_num_rows($verificarSolicitudDuplicada)>0) {
    echo'<script>
    Swal.fire({
     icon: "warning",
     title: "Ya existe una solicitud con ese paciente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
            window.history.go(-1);
        }
     });
    </script>';
    exit;
}

$resultado=mysqli_query($conexion, $insertar);
if (!$resultado) {
    echo'<script>
    Swal.fire({
     icon: "error",
     title: "Error al crear la solicitud de donación",
     text: "Intentalo nuevamente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "solicitudes.php";
        }
     });
    </script>';
} else {
    echo'<script>
    Swal.fire({
     icon: "success",
     title: "Solicitud creada, sera visible cuando sea aprobada",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "solicitudes.php";
        }
     });
    </script>';
}

mysqli_close($conexion);
?>