<header>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</header>
<style>
    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
</style>
<?php
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");

$id_rol=$_POST['rol'];
$nombre=$_POST['nombre'];
$apellido=$_POST['apellido'];
$correo=$_POST['correo'];
$telefono=$_POST['telefono'];
$tipo_sangre=$_POST['tipoSangre'];
$dni=$_POST['dni'];
$ciudad=$_POST['ciudad'];
$distrito=$_POST['distrito'];

$insertar="INSERT INTO usuarios(`id_rol`, `nombre`, `apellido`, `email`, `telefono`,`tipo_sangre`, `dni`, `ciudad`, `distrito`, `contrasena`) VALUES
('$id_rol', '$nombre', '$apellido', '$correo', '$telefono', '$tipo_sangre', '$dni', '$ciudad', '$distrito', '$dni');";

$verificarSolicitudDuplicada=mysqli_query($conexion, "SELECT * FROM solicitudes WHERE dni='$dni'");
if (mysqli_num_rows($verificarSolicitudDuplicada)>0) {
    echo'<script>
    Swal.fire({
     icon: "warning",
     title: "Ya existe una solicitud con ese paciente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
            window.history.go(-1);
        }
     });
    </script>';
    exit;
}

$resultado=mysqli_query($conexion, $insertar);
if (!$resultado) {
    echo'<script>
    Swal.fire({
     icon: "error",
     title: "Error al crear el usuario",
     text: "Intentalo nuevamente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "dashboardadmiUsuariosInsert.php";
        }
     });
    </script>';
} else {
    echo'<script>
    Swal.fire({
     icon: "success",
     title: "Usuario creado con Éxito",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "dashboardadmiUsuariosInsert.php";
        }
     });
    </script>';
}

mysqli_close($conexion);
?>