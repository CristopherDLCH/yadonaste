<header>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</header>
<style>
    body {
  font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif; 
}
</style>
<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];

$id_solicitudes=$_POST['id_solicitudes'];
$nombre=$_POST['nombre2'];
$apellido=$_POST['apellido2'];
$email=$_POST['email2'];
$telefono=$_POST['telefono2'];
$tipo_sangre=$_POST['tipo_sangre2'];
$dni=$_POST['dni2'];
$ciudad=$_POST['ciudad2'];
$distrito=$_POST['distrito2'];

$insertar="INSERT INTO donadores(`id_solicitudes`, `nombre`, `apellido`, `email`, `telefono`, `tipo_sangre`, `dni`, `ciudad`, `distrito`) VALUES
('$id_solicitudes', '$nombre', '$apellido', '$email', '$telefono', '$tipo_sangre', '$dni', '$ciudad', '$distrito');";

if ($varSesion==null || $varSesion='') {
    header('Location:index.html');
    die();
}

$verificarUsuarioDuplicado=mysqli_query($conexion, "SELECT * FROM donadores WHERE id_solicitudes='$id_solicitudes' AND dni='$dni'");
if (mysqli_num_rows($verificarUsuarioDuplicado)>0) {
    echo'<script>
    Swal.fire({
     icon: "error",
     title: "Ya haz postulado en está solicitud",
     text: "Escoge otra",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "dashboard.php";
        }
     });
    </script>';
    exit;
}

$resultado=mysqli_query($conexion, $insertar);
if (!$resultado) {
    echo'<script>
    Swal.fire({
     icon: "error",
     title: "Error al donar,
     text: "Intentalo nuevamente",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "dashboard.php";
        }
     });
    </script>';
} else {
    echo'<script>
    Swal.fire({
     icon: "success",
     title: "Postulaste a está solicitud con exito",
     showConfirmButton: true,
     allowOutsideClick: false,
     confirmButtonText: "Cerrar"
     }).then(function(result){
        if(result.value){                   
         window.location = "dashboard.php";
        }
     });
    </script>';
}

mysqli_close($conexion);
?>