<?php
session_start();
error_reporting(0);
$conexion=mysqli_connect("localhost", "root", "", "yadonaste");
$varSesion=$_SESSION['usuario'];

$consultaa="SELECT id_usuarios FROM usuarios WHERE email='$varSesion'";
$resultado=mysqli_query($conexion, $consultaa);
$row=mysqli_fetch_assoc($resultado);

if ($varSesion==null || $varSesion='') {
  header('Location:index.html');
  die();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú Principal</title>
    <link rel="stylesheet" href="../css/styleSolicitude.css">
    <link href="../img/faviconn.png" rel="icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
  </head>
  <body>

    <input type="checkbox" id="check">
    <!--header area start-->
    <header>
      <label for="check">
        <i class="fas fa-bars" id="sidebar_btn"></i>
      </label>
      <div class="left_area">
        <h3>Ya <span>Donaste</span></h3>
      </div>
      <div class="right_area">
        <a href="exitSession.php" class="logout_btn">Salir</a>
      </div>
    </header>
    <!--header area end-->
    <!--mobile navigation bar start-->
    <div class="mobile_nav">
      <div class="nav_bar">
        <img src="../img/fotoPerfil.png" class="mobile_profile_image" alt="">
        <i class="fa fa-bars nav_btn"></i>
      </div>
      <div class="mobile_nav_items">
        <a href="dashboardadmi.php"><i class="fas fa-sliders-h"></i><span>Solicitudes en espera</span></a>
        <a href="dashboardadmiSolicitudInsert.php"><i class="fas fa-sliders-h"></i><span>Crear solicitud</span></a>
        <a href="dashboardadmiSolicitudes.php"><i class="fas fa-sliders-h"></i><span>Solicitudes</span></a>
        <a href="dashboardadmiUsuariosInsert.php"><i class="fas fa-cogs"></i><span>Crear usuario</span></a>
        <a href="dashboardadmiUsuarios.php"><i class="fas fa-cogs"></i><span>Usuarios</span></a>
      </div>
    </div>
    <!--mobile navigation bar end-->
    <!--sidebar start-->
    <div class="sidebar">
      <div class="profile_info">
        <img src="../img/fotoPerfil1.png" class="profile_image" alt="">
        <h4><?php echo $_SESSION['usuario']?></h4>
        <h4>Administrador</h4>
      </div>
      <a href="dashboardadmi.php"><i class="fas fa-sliders-h"></i><span>Solicitudes en espera</span></a>
        <a href="dashboardadmiSolicitudInsert.php"><i class="fas fa-sliders-h"></i><span>Crear solicitud</span></a>
        <a href="dashboardadmiSolicitudes.php"><i class="fas fa-sliders-h"></i><span>Solicitudes</span></a>
        <a href="dashboardadmiUsuariosInsert.php"><i class="fas fa-cogs"></i><span>Crear usuario</span></a>
        <a href="dashboardadmiUsuarios.php"><i class="fas fa-cogs"></i><span>Usuarios</span></a>
    </div>
    <!--sidebar end-->

    <div class="content">
      <div class="card">
        <h1>Te ayudamos a llegar a más potenciales donantes de sangre</h1>
        <p>Crea una solicitud de donación de sangre con los datos requeridos para que en poco tiempo se contacten
        contigo.</p>
      </div>
      <div class="mitos">
        <div class="mitosPartes"> 
          <form action="dashboardadmiSolicitudInsert2.php" method="POST">
            <input type="hidden" class="uno" name="idUsuarios" value="<?php echo $row["id_usuarios"]?>">
            <input type="text" id="login" class="dos" name="nombre" onkeypress="return SoloLetras(event);" placeholder="Nombre" required>
            <input type="text" id="login" class="dos" name="apellido" onkeypress="return SoloLetras(event);" placeholder="Apellido" required>
            <input type="number" id="login" min="18" max="99" maxlength="2" class="dos validar" name="edad" placeholder="Edad" required>
            <br>
            <select type="text" id="login" class="selectt" name="tipoSangre" placeholder="Tipo de sangre" required>
            <option value="">Tipo de Sangre</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>   
             </select>        
            <select type="number" id="login" class="selectt" name="cantidadDonantes" placeholder="Cantidad de donantes" required>
            <option value="">N° de donantes</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
            </select>   
            <input type="text" id="login"  minlength="8" maxlength="8" class="dos validar" name="dni" placeholder="DNI" required>
            <br>
            <input type="text" id="login" class="dos" name="ciudad" value="Lima" placeholder="Ciudad">
            <select type="text" id="login" class="selectt" name="distrito" placeholder="Distrito"required>
            <option value="">Distrito</option>
                    <option value="Cercado de Lima">Cercado de Lima</option>
                    <option value="Ate">Ate</option>
                    <option value="Barranco">Barranco</option>
                    <option value="Breña">Breña</option>
                    <option value="Comas">Comas</option>
                    <option value="Chorrillos">Chorrillos</option>
                    <option value="Agustino">El Agustino</option>
                    <option value="Jesús María">Jesús María</option>
                    <option value="La Molina">La Molina</option>
                    <option value="La Victoria">La Victoria</option>
                    <option value="Lince">Lince</option>
                    <option value="Magdalena">Magdalena</option>
                    <option value="Miraflores">Miraflores</option>
                    <option value="Pueblo Libre">Pueblo Libre</option>
                    <option value="Puente Piedra">Puente Piedra</option>
                    <option value="Rimac">Rimac</option>
                    <option value="San Isidro">San Isidro</option>
                    <option value="Independencia">Independencia</option>
                    <option value="San Juan de Miraflores">San Juan de Miraflores</option>
                    <option value="San Luis">San Luis</option>
                    <option value="San Martin de Porres">San Martin de Porres</option>
                    <option value="San Miguel">San Miguel</option>
                    <option value="Santiago de Surco">Santiago de Surco</option>
                    <option value="Surquillo">Surquillo</option>
                    <option value="Villa María del Triunfo">Villa María del Triunfo</option>
                    <option value="San Juan de Lurigancho">San Juan de Lurigancho</option>
                    <option value="Santa Rosa">Santa Rosa</option>
                    <option value="Los Olivos">Los Olivos</option>
                    <option value="San Borja">San Borja</option>
                    <option value="Villa El Savador">Villa El Savador</option>
                    <option value="Santa Anita">Santa Anita</option>
            </select>
            
            <select type="text" id="login" class="selectt" name="hospital" placeholder="Hospital de la donación" required>
            <option value="">Hospital de donación</option>
                    <option value="Hosp. Nacional Arzobispo Loaysa">Hosp. Nacional Arzobispo Loaysa</option>
                    <option value="Hosp. Nacional Dos de Mayo">Hosp. Nacional Dos de Mayo</option>
                    <option value="Hosp. Nacional Docente Madre - Niño San Bartolome">Hosp. Nacional Docente Madre - Niño San Bartolome</option>
                    <option value="Hosp. Santa Rosa">Hosp. Santa Rosa</option>
                    <option value="Hosp. de Emergencias Jose Casimiro Ullosa">Hosp. de Emergencias Jose Casimiro Ullosa</option>
                    <option value="Hosp. de Emergencias Pediatricas">Hosp. de Emergencias Pediatricas</option>
                    <option value="Hosp. Victor Larco Herrera">Hosp. Victor Larco Herrera</option>
                    <option value="Hosp. Nacional Sergio E. Bernales">Hosp. Nacional Sergio E. Bernales</option>
                    <option value="Hosp. Nacional Cayetano Heredia">Hosp. Nacional Cayetano Heredia</option>
                    <option value="Hosp. Carlos Lanfranco La Hoz">Hosp. Carlos Lanfranco La Hoz</option>
                    <option value="Hosp. Maria Auxiliadora">Hosp. Maria Auxiliadora</option>
                    <option value="Hosp. Nacional Hipolito Unanue">Hosp. Nacional Hipolito Unanue</option>
                    <option value="Hosp. Vitarte">Hosp. Vitarte</option>
                    <option value="Hosp. Jose Agurto Tello de Chosica">Hosp. Jose Agurto Tello de Chosica</option>
                    <option value="Hosp. San Juan de Lurigancho">Hosp. San Juan de Lurigancho</option>
                    <option value="Hosp. Huaycan">Hosp. Huaycan</option>
                    <option value="Hosp. Emergencias Villa el Salvador">Hosp. Emergencias Villa el Salvador</option>
            </select>
            <br>
            <input type="text" id="login" class="dos" maxlength="100" name="descripcion" onkeypress="return Solotextt(event);" placeholder="Descripción del paciente (max 100 carácteres)" required>
            <input type="text" id="login" minlength="9" maxlength="9" class="dos validar" name="telefono" placeholder="Teléfono" required>       
            <select type="text" id="login" class="selectt" name="estado" placeholder="Estado" required>
                    <option value="">Estado</option>
                    <option value="Aprobado">Aprobado</option>
                    <option value="No aprobado">No aprobado</option>
                    <option value="Denegado">Denegado</option>
            </select>
            <br>
            <input type="submit" class="fadeIn fourth" value="Crear solicitud">
          </form>
        </div> 
      </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
      $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
      });
    });

    function SoloLetras(e)
    {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toString();
      letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚñÑ ";

      especiales = [8,13];
      tecla_especial = false
      for(var i in especiales) {
      if(key == especiales[i]){
      tecla_especial = true;
      break;
      }
      }

      if(letras.indexOf(tecla) == -1 && !tecla_especial)
      {
      alert("Ingresar solo letras");
      return false;
      }
    }
    /**VALIDACION DE DESCRICPCION EN EL PACIENTE**/ 
    function Solotextt(e)
    {
      key = e.keyCode || e.which;
      tecla = String.fromCharCode(key).toString();
      letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzáéíóúÁÉÍÓÚñÑ1234567890´´.,# ";

      especiales = [8,13];
      tecla_especial = false
      for(var i in especiales) {
      if(key == especiales[i]){
      tecla_especial = true;
      break;
      }
      }

      if(letras.indexOf(tecla) == -1 && !tecla_especial)
      {
      alert("Porfavor no Ingresar Caracteres especiales");
      return false;
      }
    }

    $(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});
    </script>

  </body>
</html>