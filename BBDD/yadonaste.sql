-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-09-2021 a las 05:14:34
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `yadonaste`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarSolicitud` (IN `_idsolicitudes` INT, IN `_nombrepaciente` VARCHAR(250), IN `_tiposangre` VARCHAR(250), IN `_cantidaddonantes` INT, IN `_ciudad` VARCHAR(250), IN `_hospitaldonacion` VARCHAR(250), IN `_descripcion` VARCHAR(250), IN `_telefono` VARCHAR(250), IN `_detalles` VARCHAR(250))  BEGIN
	UPDATE solicitudes SET nombre_paciente = _nombrepaciente, tipo_sangre = _tiposangre, cantidad_donantes = _cantidaddonantes,
    ciudad = _ciudad, hospital_donacion = _hospitaldonacion, descripcion = _descripcion,
    telefono = _telefono, detalles = _detalles WHERE id_solicitudes = _idsolicitudes;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarUsuario` (IN `_idusuarios` INT, IN `_nombres` VARCHAR(45), IN `_apellidos` VARCHAR(45), IN `_email` VARCHAR(45), IN `_telefono` VARCHAR(45), IN `_tiposangre` VARCHAR(45), IN `_ciudad` VARCHAR(45), IN `_contrasena` VARCHAR(45))  BEGIN
	UPDATE usuarios SET nombres = _nombres, apellidos = _apellidos, email = _email, telefono = _telefono,
    tipo_sangre = _tiposangre, ciudad = _ciudad, contrasena = _contrasena WHERE id_usuarios = _idusuarios;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarSolicitud` (IN `_idsolicitudes` INT)  BEGIN
	DELETE FROM solicitudes WHERE id_solicitudes = _idsolicitudes;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminarUsuario` (IN `_idusuarios` INT)  BEGIN
	DELETE FROM usuarios WHERE id_usuarios = _idusuarios;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertarSolicitud` (IN `_idusuarios` INT, IN `_nombrepaciente` VARCHAR(250), IN `_tiposangre` VARCHAR(250), IN `_cantidaddonantes` INT, IN `_ciudad` VARCHAR(250), IN `_hospitaldonacion` VARCHAR(250), IN `_descripcion` VARCHAR(250), IN `_telefono` VARCHAR(250), IN `_detalles` VARCHAR(250))  BEGIN
	INSERT INTO solicitudes (id_usuarios, nombre_paciente, tipo_sangre, cantidad_donantes, ciudad, hospital_donacion, descripcion,
    telefono, detalles)
	VALUES (_idusuarios, _nombrepaciente, _tiposangre, _cantidaddonantes, _ciudad, _hospitaldonacion, _descripcion, _telefono,
    _detalles);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertarUsuario` (IN `_nombres` VARCHAR(45), IN `_apellidos` VARCHAR(45), IN `_email` VARCHAR(45), IN `_telefono` VARCHAR(45), IN `_tiposangre` VARCHAR(45), IN `_ciudad` VARCHAR(45), IN `_contrasena` VARCHAR(45))  BEGIN
	INSERT INTO usuarios (nombres, apellidos, email, telefono, tipo_sangre, ciudad, contrasena)
	VALUES (_nombres, _apellidos, _email, _telefono, _tiposangre, _ciudad, _contrasena);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarSolicitud` (IN `_idsolicitudes` INT)  BEGIN
	select * from solicitudes where id_solicitudes =  _idsolicitudes;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarSolicitudes` ()  BEGIN
	select * from solicitudes;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuario` (IN `_idusuarios` INT)  BEGIN
	select * from usuarios where id_usuarios =  _idusuarios;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListarUsuarios` ()  BEGIN
	select * from usuarios;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `VerificarLogin` (IN `_email` VARCHAR(255), IN `_contrasena` VARCHAR(255))  BEGIN
	SELECT * FROM usuarios WHERE email = _email AND contrasena = _contrasena;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donadores`
--

CREATE TABLE `donadores` (
  `id_donador` int(11) NOT NULL,
  `id_solicitudes` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tipo_sangre` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `dni` int(8) NOT NULL,
  `ciudad` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `distrito` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `donadores`
--

INSERT INTO `donadores` (`id_donador`, `id_solicitudes`, `nombre`, `apellido`, `email`, `telefono`, `tipo_sangre`, `dni`, `ciudad`, `distrito`) VALUES
(17, 2, 'pedro', 'luna', 'luna@gmail.com', '912157223', 'AB-', 89564654, 'Lima', 'Independencia'),
(18, 1, 'pedro', 'luna', 'luna@gmail.com', '912157223', 'AB-', 89564654, 'Lima', 'Independencia'),
(19, 1, 'juan', 'bellido', 'bellido@gmail.com', '956646545', 'A-', 89879879, 'Lima', 'Rimac'),
(20, 7, 'nestor', 'sanchez', 'nestor@gmail.com', '985632589', 'O+', 65945894, 'Lima', 'Rimac'),
(21, 4, 'pedro', 'luna', 'luna@gmail.com', '912157223', 'AB-', 89564654, 'Lima', 'Independencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `nombre_rol` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `nombre_rol`) VALUES
(1, 'administrador'),
(2, 'analista'),
(3, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

CREATE TABLE `solicitudes` (
  `id_solicitudes` int(11) NOT NULL,
  `id_usuarios` int(11) NOT NULL,
  `nombre` varchar(25) CHARACTER SET utf8mb4 DEFAULT NULL,
  `apellido` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `edad` int(122) NOT NULL,
  `tipo_sangre` varchar(25) CHARACTER SET utf8mb4 DEFAULT NULL,
  `cantidad_donantes` int(10) DEFAULT NULL,
  `dni` int(8) NOT NULL,
  `ciudad` varchar(25) CHARACTER SET utf8mb4 DEFAULT NULL,
  `distrito` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `hospital_donacion` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `descripcion` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `telefono` varchar(250) CHARACTER SET utf8mb4 DEFAULT NULL,
  `estado` enum('Aprobado','No aprobado','Denegado','') COLLATE utf8mb4_spanish_ci NOT NULL,
  `proceso` enum('Completo','Incompleto','','') COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `solicitudes`
--

INSERT INTO `solicitudes` (`id_solicitudes`, `id_usuarios`, `nombre`, `apellido`, `edad`, `tipo_sangre`, `cantidad_donantes`, `dni`, `ciudad`, `distrito`, `hospital_donacion`, `descripcion`, `telefono`, `estado`, `proceso`) VALUES
(1, 3, 'maria', 'vasquez', 58, 'A-', 2, 69854789, 'Lima', 'Miraflores', 'Hosp. Victor Larco Herrera', 'se encuentra en estado critico', '334434342', 'Aprobado', 'Incompleto'),
(2, 1, 'flor', 'paredes', 59, 'B+', 3, 32546465, 'Lima', 'Pueblo Libre', 'Hosp. Vitarte', 'esta hospitalizada', '956456842', 'Aprobado', 'Incompleto'),
(3, 1, 'sebastian', 'torres', 31, 'A+', 2, 12345678, 'Lima', 'El Agustino', 'Hosp. Maria Auxiliadora', 'tiene problemas respiratorios', '999123654', 'Denegado', 'Incompleto'),
(4, 1, 'Almendra', 'Buggari', 53, 'A-', 1, 63258747, 'Lima', 'Miraflores', 'Hosp. Nacional Arzobispo Loaysa', 'es una persona muy nerviosa', '985325698', 'Aprobado', 'Incompleto'),
(7, 4, 'mariella', 'bellido', 33, 'B+', 2, 32584589, 'Lima', 'Chorrillos', 'Hosp. Emergencias Villa el Salvador', 'es una persona con dolores', '998574598', 'Aprobado', 'Incompleto'),
(8, 1, 'estrella', 'paredes', 26, 'A-', 3, 78856876, 'Lima', 'Breña', 'Hosp. Carlos Lanfranco La Hoz', 'Es una persona en silla de ruedas', '998973513', 'No aprobado', 'Completo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuarios` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `nombre` varchar(25) CHARACTER SET utf8mb4 NOT NULL,
  `apellido` varchar(25) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `telefono` varchar(9) CHARACTER SET utf8mb4 NOT NULL,
  `tipo_sangre` varchar(25) CHARACTER SET utf8mb4 NOT NULL,
  `dni` int(8) NOT NULL,
  `ciudad` varchar(25) CHARACTER SET utf8mb4 NOT NULL,
  `distrito` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `contrasena` varchar(50) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuarios`, `id_rol`, `nombre`, `apellido`, `email`, `telefono`, `tipo_sangre`, `dni`, `ciudad`, `distrito`, `contrasena`) VALUES
(1, 1, 'cristopher', 'de las casas', 'cristopher@gmail.com', '985102325', 'B+', 2236578, 'Lima', 'Rimac', '123456'),
(2, 3, 'alejandro', 'chevez', 'alejandro@gmail.com', '965897874', 'A+', 39948948, 'Lima', 'Independencia', '123456'),
(3, 3, 'nestor', 'sanchez', 'nestor@gmail.com', '985632589', 'O+', 65945894, 'Lima', 'Rimac', '123456'),
(4, 3, 'pedro', 'luna', 'luna@gmail.com', '912157223', 'AB-', 89564654, 'Lima', 'Independencia', '12345'),
(5, 3, 'juan', 'bellido', 'bellido@gmail.com', '956646545', 'A-', 89879879, 'Lima', 'Rimac', '123456'),
(6, 3, 'alejo', 'perez', 'ale@gmail.com', '965587455', 'O-', 5569855, 'Lima', 'Lince', '123456'),
(7, 3, 'felipe', 'savedra', 'felipe@gmail.com', '985566998', 'B+', 5588966, 'Lima', 'Lince', ''),
(8, 3, 'sebastian', 'lizarza', 'sebas@gmail.com', '984239849', 'B+', 56156948, 'Lima', 'San Juan de Miraflores', '56156948');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `donadores`
--
ALTER TABLE `donadores`
  ADD PRIMARY KEY (`id_donador`),
  ADD KEY `id_solicitudes` (`id_solicitudes`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD PRIMARY KEY (`id_solicitudes`),
  ADD KEY `id_usuarios` (`id_usuarios`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuarios`),
  ADD KEY `usuarios_ibfk_1` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `donadores`
--
ALTER TABLE `donadores`
  MODIFY `id_donador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  MODIFY `id_solicitudes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `donadores`
--
ALTER TABLE `donadores`
  ADD CONSTRAINT `donadores_ibfk_1` FOREIGN KEY (`id_solicitudes`) REFERENCES `solicitudes` (`id_solicitudes`);

--
-- Filtros para la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
  ADD CONSTRAINT `solicitudes_ibfk_1` FOREIGN KEY (`id_usuarios`) REFERENCES `usuarios` (`id_usuarios`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
